from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

#serializers
from polls.api.serializers import UserSerializers
from polls.models import User

#token
from rest_framework.decorators import  permission_classes
from rest_framework.permissions import IsAuthenticated

@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def user_api_view(request):
    if request.method == 'GET':
        _user = User.objects.all()
        user_serializers = UserSerializers(_user, many=True)
        return Response(user_serializers.data, status=status.HTTP_200_OK)

    if request.method == 'POST':
        user_serializers = UserSerializers(data=request.data)
        if user_serializers.is_valid():
            user_serializers.save()
            return Response({'Message': 'Created Successfully'}, status=status.HTTP_201_CREATED)
        return Response(user_serializers.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','PUT','DELETE'])
def user_detail_view(request, pk):
    users = User.objects.filter(id=pk)

    if users.exists():
        users = users.latest('id')

        if request.method == 'GET':
            users_serializers = UserSerializers(users)
            return Response(users_serializers.data, status=status.HTTP_200_OK)

        if request.method == 'PUT':
            users_serializers = UserSerializers(users, data=request.data)
            if users_serializers.is_valid():
                users_serializers.save()
                return Response({'Message: ': 'The user was updated'}, status=status.HTTP_200_OK)
            return Response(users_serializers.errors, status=status.HTTP_400_BAD_REQUEST)

        if request.method == 'DELETE':
            users.delete()
            return Response({'Message: ': 'The user was deleted'}, status=status.HTTP_200_OK)

    return Response({'Message: ': 'The user does not exist'}, status.HTTP_404_NOT_FOUND)










