from django.urls import path
from polls.api.api import user_api_view, user_detail_view

urlpatterns = [
    path('serializer_user/', user_api_view, name='user'),
    path('serializer_user/<int:pk>', user_detail_view, name='user_id'),
]

#http://127.0.0.1:8000/user/serializer_user/