from django.db import models

class User(models.Model):
    id=models.AutoField(primary_key=True)
    name=models.CharField(max_length=200)
    last_name=models.CharField(max_length=200)
    phone=models.IntegerField(blank=False)
    email=models.EmailField(max_length=150)
    address=models.TextField(max_length=200)

    def __str__(self):
        return self.name

